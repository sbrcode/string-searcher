#! /usr/bin/env python3

import re, os, argparse

# Retrieving Arguments with Error and Suffix Management
parser = argparse.ArgumentParser(description="Extract all file strings from the source folder as an argument.")
parser.add_argument("rootdir", help="Relative or absolute path")
parser.add_argument("-s", "--suffix", help="limit search to suffix files")
parser.add_argument("-p", "--path", action='store_true', help="each line produced is preceded \
by the associated file path followed by a tabulation")
args = parser.parse_args()

# for loop with each option
for path, dirs, files in os.walk(args.rootdir):
    for filename in files:
        # suffix management
        if not args.suffix or filename.endswith(args.suffix):
            # complete path file constitution
            pathfilename = os.path.join(path, filename)
            with open(pathfilename, 'r', errors='ignore') as filex:
                for line in filex:
                    results = re.findall(r'"[^"]*" | \'[^\']*\'', line, re.U)
                    for result in results:
                        # path management
                        if args.path:
                            print(pathfilename, result, sep='\t\t')
                        # without options
                        else:
                            print(result)