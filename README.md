## strextract
Python3 program that recursively traverses all files (source) of hierarchy<br>
and produced under all literal strings that contain these files.<br>
The strings will be displayed as they were in the file, surrounded by the same "or" symbols.<br>
By default, hidden files (those whose names begin with '.') Are not taken into account.<br>


## usage:
strsearch [-h] [-s SUFFIX] [-p] rootdir

Extract all file strings from the source folder as an argument.<br>
positional arguments:
* rootdir<br>                      Relative or absolute path

optional arguments:
* -h, --help<br>                   show this help message and exit
* -s SUFFIX, --suffix SUFFIX<br>   limit search to suffix files
* -p, --path<br>                   each line produced is preceded by the associated file, path followed by a tabulation